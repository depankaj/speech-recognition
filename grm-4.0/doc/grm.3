.TH GRM 3 "" "Version 4.0"

.SH NAMES
GRMCfCompile, GRMCdCompile, GRMCountNgrams, GRMEdit, GRMFailure, GRMLocalDeterminize, GRMLocalGrammar, GRMMake, GRMMerge, GRMModelConvert, GRMModelShrink,
GRMMutate, GRMRead, GRMReplace, GRMSubstitute, GRMSuffix - GRM C library
.PP
.SH SYNOPSIS
\fB#include "grm.h"
.PP
\fBFsm GRMCdCompile(Fsm fsm1, Fsm fsm2, Fsm fsm3, Fsm fsm4, Fsm fsm5,
.PP
.RS 1.3in
\fB GRMLabelSet *sigma, int direction, int option, FSMModes mode)
.RE
.PP
\fBFsm GRMCfCompile(Fsm fsm, char *symbols, int linear, FSMModes mode)
.PP
\fBFsm GRMCountNgrams(Fsm ifsm, Fsm cfsm, int order, int symbol, int fsymbol, FSMModes mode)
.PP
\fBFsm GRMEdit(Fsm fsm, int initial, int final, Cost cost, FSMModes
mode)
.PP
\fBFsm GRMFailure(Fsm fsm, int phi, int side)
.PP
\fBFsm GRMLocalDeterminize(Fsm fsm, int threshold, FSMModes mode)
.PP
\fBFsm GRMLocalGrammar(Fsm fsm, int phi, FSMModes mode)
.PP
\fBFsm GRMMake(Fsm fsm, int order, unsigned int props, FSMModes mode)
.PP
\fBFsm GRMMerge(Fsm fsm1, Fsm fsm2, FSMModes mode)
.PP
\fBFsm GRMModelShrink(Fsm fsm, double factor, unsigned props, FSMModes mode)
.PP
\fBFsm GRMModelConvert(Fsm fsm, int fsymbol, int psymbol, double cost, unsigned props, FSMModes mode)
.PP
\fBFsm GRMMutate(Fsm fsm1, Fsm fsm2, FSMModes mode)
.PP
\fBFsm GRMReplace(Fsm fsm1, GRMFsmArray fsmarray, FSMModes mode)
.PP
\fBFsm GRMSubstitute(Fsm fsm, GRMList list, FSMModes mode)
.PP
\fBFsm GRMRead(char *file, int weighted, char *symbols, int compound)
.PP
\fBFsm GRMSuffix(Fsm fsm, FSMModes mode)
.PP
.SH DESCRIPTION
These functions  construct, modify, and compile rules or grammars into
.I weighted acceptors
and
.I weighted transducers
(see the FSM Library for the definitions of the components of an FSM:
state, start state, arc (transition), input symbol, output symbol, arc
cost, final cost and final state.).
.PP
Many of these functions can operate in three different modes -
.I constructive,
.I destructive,
and
.I delayed, 
specified by the 
.I FSMModes
argument values
.I FSMNorm,
.I FSMDestruct,
and
.I FSMDelay,
respectively. See the FSM library for the definition of these modes.
.PP
For convenience, all functions that have an 
.I FSMModes 
argument can take all three modes as input. However, the
specification of this argument should be viewed merely as licensing
the function to reuse its input or delay its output, and not as a
guarantee that the algorithm(s) used to implement the function will,
in fact, do so. For example, a function may be implemented solely by
a destructive algorithm, and if given any mode other than
.I FSMDestruct 
or if given non-mutable input FSM, it might simply copy the input FSM first.
For each function that takes an
.I FSMModes
argument, we indicate (in parentheses) which
kind(s) of algorithm are indeed used to implement it.
The experienced user may wish to select a mode that matches these capabilities
to obtain the greatest efficiency. On the other hand, the novice can
simply use
.I FSMNorm,
since this mode does not require the care in storage and data-flow
management that the others do. More specific indications regarding the
use of the \fIFSMModes\fR argument is given below for each function.
.SS COMPILATION OF CONTEXT-DEPENDENT REWRITE RULES
.I GRMCdCompile
takes as input four FSMs:
.I fsm1, fsm2, fsm3 
and 
.I fsm4, 
and returns a weighted transducer representing the context-dependent
rewrite rule:
.PP
.RS 3
.I fsm1 -> fsm2 / fsm3___fsm4.
.RS -3
.PP
The four input FSMs,
.I fsm1, fsm2, fsm3 
and
.I fsm4,
must be acceptors. 
.I fsm1, fsm3
and
.I fsm4
must be costless. The definition of the rule is that a string \fIx\fR accepted by
\fIfsm1\fR is replaced by the (weighted) language given by \fIfsm2\fR
in the context where it is preceded by a string of \fIfsm3\fR and
followed by a string of \fIfsm4\fR. The application of the rule
proceeds from left to right if \fIdirection = 1\fR, from right to left
if \fIdirection = -1\fR and applies in parallel or simultaneously if
\fIdirection = 0\fR.  The application of the rule is optional if
\fIoption = 1\fR, obligatory if \fIoption = 0\fR. \fIsigma\fR
represents the alphabet of the set of symbols of the rule. The 
format of \fIsigma\fR is defined by the following structure:
.PP
.Cs
typedef struct {
  GRMHashTable sym;
  int *lab;
  int nlab;
  int max;
} GRMLabelSet;
.Ce
.PP
\fIsym\fR is a hash table used to store the mapping from textual
representation of symbols to non-negative integers. \fIlab\fR is the
array of symbols of the alphabet. \fInlab\fR gives the size of the
array. \fImax\fR is the maximum symbol of the alphabet. The
\fIFSMModes\fR argument applies to all five input FSMs. (Algorithm:
destructive)
.PP
.SS COMPILATION OF CONTEXT-FREE GRAMMARS
.I GRMRead
loads a file representing a (weighted) context-free grammar from
.I file
and outputs an FSM encoding that grammar. If \fIfile =\fR NULL, it
reads from the standard input. The input file
.I file
must be the textual representation of a (weighted) context-free
grammar (see \fIgrm(5)\fR). 
\fIcompound\fR must be set to 1 to allow the use of compound rules in
the representation of the input grammar (see \fIgrm(5)\fR), to 0
otherwise. Input symbols are represented in the input file by
non-negative numbers, unless the file named
.I symbols
is not NULL. This allows non-terminal and terminal symbols to be
given textual names, where the file
.I symbols 
gives the translation from names to numbers (see \fI fsm(5)\fR and \fI
grm(5)\fR). For efficiency reasons, it is recommended that a
consecutive numbering be used with non-terminal symbols coming before
terminal symbols in that ordering. The input must be an unweighted
context-free grammar, unless 
.I weighted = 1,
in which case it must be a weighted context-free grammar.
.PP
.I GRMCfCompile
accepts as input an FSM output by \fIgrmread\fR which is a binary
representation of a weighted context-free grammar \fIG\fR, and returns
a delayed (weighted) acceptor recognizing \fIG\fR. The grammar \fIG\fR
must be \fIstrongly regular\fR, i.e., the rules of each set \fIM\fR of
mutually recursive nonterminals are either all right-linear or all
left-linear (nonterminals that do not belong to \fIM\fR are considered
as terminals for deciding if a rule of \fIM\fR is right- or
left-linear). When the input grammar is not strongly regular, the list
of the rules of the first set \fIM\fR of mutually recursive
nonterminals found by the algorithm to violate the strong regularity
condition is sent to the standard error. The file named
.I symbols 
gives the translation from numbers to names (see \fI fsm(5)\fR and \fI
grm(5)\fR) used to print these non-terminals. When the input grammar
is known to be either right-linear or left-linear, the argument
.I linear
can be set to 1 for a more efficient compilation.
.I GRMCfCompile
returns a \fIReplace class\fR \fIdelayed acceptor\fR. This allows one
to modify some of the characteristics of the context-free grammar --
such as the choice of the set of initial non-terminals for instance --
without having to recompile the grammar (see the sections \fIThe
Replace Class\fR and \fIDynamic modifications\fR below). The
\fIFSMModes\fR argument applies to the input FSM
\fIfsm\fR. (Algorithm: delayed)
.PP
.SS THE EDIT CLASS
.PP
The 
.I Edit class
is an FSM class defined by the GRM library. It allows users to
dynamically modify the initial state of an FSM or the final cost of a
state of an FSM. An Edit class object represents an FSM \fIF\fR
defined by:
.RS 3
.PP
1. An input FSM \fIS\fR.
.PP
2. A new initial state \fIs\fR, a state \fIf\fR, and a final cost
\fIc\fR.
.RS -3
.PP
The FSM \fIF\fR is exactly the FSM obtained from \fIS\fR by the
following delayed operations: the state \fIs\fR becomes the new
initial state if \fIs\fR is not equal to \fIFSMNoState\fR, and the
final cost \fIc\fR is assigned to \fIf\fR if \fIf\fR is not equal to
\fIFSMNoState\fR.
.PP
.I GRMEdit
takes as input an FSM \fIfsm\fR, a new initial state
\fIinitial\fR, a state \fIfinal\fR, a cost \fIcost\fR, and returns an
FSM \fIF\fR which belongs to the GRM Edit class. \fIF\fR is the
FSM obtained from \fIfsm\fR by making the state \fIinitial\fR
initial if \fIinitial\fR is not equal to \fIFSMNoState\fR, and by
assigning to the state \fIfinal\fR the final cost \fIcost\fR if
\fIfinal\fR is not equal to \fIFSMNoState\fR. The \fIFSMModes\fR
argument applies to the input FSM \fIfsm\fR. (Algorithm: delayed)
.PP
.SS THE REPLACE CLASS
.PP
The 
.I Replace class
is an FSM class defined by the GRM library. It allows users to
construct FSMs with labels referring to other FSMs.  A Replace class
object represents an FSM \fIF\fR defined by:
.RS 3
.PP
1. An FSM \fIS\fR that gives the general structure of \fIF\fR.
.PP
2. An array of pointers to FSMs, \fIT[i]\fR, indexed with the set of
all symbols \fIi\fR -- the set must include the input labels of
\fIS\fR.
.RS -3
.PP
The FSM \fIF\fR is exactly the result of the replacement by
\fI*T[i]\fR of each label \fIi\fR of \fIS\fR such that \fI*T[i]\fR is
not null. These replacements are done on-demand for a given
input. \fIF\fR is a transducer when \fIS\fR is a transducer. However,
only the input labels of \fIS\fR are then used for the replacements --
the output label of a transition of \fIS\fR will not appear in the
resultant FSM \fIF\fR if the corresponding input label is
replaced. In the case of Replace class acceptors representing
context-free grammars, typically \fI*T[i]\fR is not null for each
\fIi\fR corresponding to a non-terminal symbol, and \fIS\fR is an
acceptor accepting exactly the set of initial non-terminals \fIj\fR of
the grammar.
.PP
.SS DYNAMIC MODIFICATIONS
.I GRMReplace
takes as input an FSM \fIfsm1\fR and an array of FSMs
\fIfsmarray\fR and returns a delayed FSM \fIF\fR which belongs to the GRM
Replace class. The format of \fIfsmarray\fR is defined by the following
structure:
.PP
.Cs
typedef struct{
  Fsm *fsm;
  int nfsm;
  int refcnt;
  int *opt;
  SYSHandle hdl;
} *GRMFsmArray;
.Ce
.PP
which includes a field corresponding to an array of FSMs \fIfsm\fR,
the size of the array \fInfsm\fR, and a reference counter \fIrefcnt\fR
that stores the number of times the array \fIfsmarray\fR is used by
GRM Replace class FSMs. \fIF\fR is the result of the replacement by
\fIfsm[i]\fR of each transition of \fIfsm1\fR labeled with
\fIi\fR such that \fIfsm[i]\fR is not NULL. The other transitions of
\fIfsm1\fR remain unchanged. \fIF\fR can then be modified using
delayed operations (see the \fIDynamic Modifications\fR below). The
\fIFSMModes\fR argument applies to the input FSM \fIfsm1\fR and the
array structure \fIfsmarr\fR. Any modification of \fIfsmarr\fR affects
the resultant FSM \fIF\fR unless the mode \fIFSMNorm\fR is
used. (Algorithm: delayed)
.PP
.I GRMMutate
takes as input two FSMs \fIfsm1\fR and \fIfsm2\fR and returns the FSM
obtained by replacing the \fIstructure\fR FSM of \fIfsm2\ by
\fIfsm1\fR. \fIfsm2\fR must belong to the Replace class. 
The output of
.I GRMMutate
is a delayed FSM that has the Replace class format. A typical
application of
.I GRMMutate
is the dynamic modification of the initial non-terminal symbols of a
context-free grammar -- this is sometimes called \fIdynamic
activation\fR or \fInon-activation\fR of the rules of a context-free
grammar. \fIfsm1\fR is then a (weighted) acceptor recognizing exactly
the new initial non-terminals and \fIfsm2\fR usually the output of
.I GRMCfCompile.
The \fIFSMModes\fR argument applies to the two input FSMs \fIfsm1\fR
and \fIfsm2\fR. (Algorithm: delayed)
.PP
.I GRMSubstitute
takes as input a substitution
.I list
and an FSM \fIfsm\fR and returns the FSM obtained by
substituting FSMs for the symbols of \fIfsm\fR using the mapping
defined by the substitution list. \fIfsm\fR must belong to the Replace
class. A substitution list is an object that has the following
structure:
.PP
.Cs
typedef struct {
  GRMLabelFsm *lfsm;
  int nfsm;
} *GRMList;
.Ce
.PP
where \fIlfsm\fR is an array of objects with the following structure:
.PP
.Cs
typedef struct{
  int label;
  Fsm fsm;
} GRMLabelFsm;
.Ce
.PP
and \fInfsm\fR the size of the array.  Each element of the array
defines the mapping of a symbol \fIlabel\fR to an FSM
\fIfsml\fR. The output of
.I GRMSubstitute
is a delayed FSM that has the Replace class format. A typical
application of
.I GRMSubstitute
is the dynamic replacement of the terminal symbols of a context-free
grammar -- this is sometimes called \fI use of dynamic
lists\fI. \fIfsm\fR is then usually the output of
.I GRMCfCompile.
Note that the substitution for the non-terminal symbols of a
context-free grammar is also allowed but the user should be aware that
this can have subtle effects. The substitution for a non-terminal
\fIX\fR does not affect a non-terminal \fIY\fR -- or equivalently the
rules defining \fIY\fR -- if \fIX\fR and \fIY\fR are mutually
dependent. The \fIFSMModes\fR argument applies to the input FSM
\fIfsm\fR. (Algorithm: delayed)
.PP
.SS FAILURE CLASS
The
.I Failure, Input Failure 
and
.I Output Failure
classes are FSM classes defined by the GRM library. 
A Failure class object represents an FSM \fIF\fR defined by:
.RS 3
.PP
1.  A special symbol \fIphi\fR.
.PP
2. An acceptor \fIS\fR containing no more than one transition 
labeled by \fIphi\fR leaving any state.
.RS -3
.PP
The FSM \fIF\fR is the acceptor obtained from \fIS\fR by interpreting
transitions labeled with \fIphi\fR as failure transitions, \fIi.e.\fR,
the set of transitions labeled with \fIl\fR leaving the current
state is recursively defined by:
.RS 3
.PP
- the set of transitions labeled with \fIl\fR leaving the current
state \fIq\fR if that set is not empty or if no \fIphi\fR transition
leaves \fIq\fR;
.PP
- the set of transitions labeled with \fIl\fR leaving the destination
state of the unique \fIphi\fR transition of \fIq\fR otherwise.
.RS -3
.PP
For transducers, two classes are similarily defined:
the \fIInput Failure class\fR where transitions with input
label \fIphi\fR are interpreted as failure transitions,
and the \fIOutput Failure class\fR where transitions with output
label \fIphi\fR are interpreted as failure transitions.
Failure, Input Failure and Output Failure class FSMs
are indexed, input indexed and output indexed FSMs, 
\fIi.e.\fR they support the FSMSeekLabel operation on their
labels, input labels and output labels respectively.
The only operations allowed for any FSM \fIF\fR belonging to theses classes
are composition and intersection with non-indexed FSMs
on the indexed side of \fIF\fR.
.PP
.I GRMFailure
converts the input FSM \fIfsm\fR to a failure class representation
interpreting transitions labeled with \fIphi\fR as failure transitions.
If \fIfsm\fR is an acceptor, it is converted to the Failure class.
If \fIfsm\fR is a transducer, it is converted to the Input Failure
class if \fIside = 1\fR and to the Output Failure class if \fIside = 2\fR.
.PP
.SS TEXT AND GRAMMAR PROCESSING UTILITIES
.I GRMLocalGrammar 
takes as input an acceptor \fIfsm\fR
and a label \fIphi\fR and returns a Failure class acceptor.
\fIfsm\fR must be deterministic and unweighted.
The resulting acceptor is deterministic and unweighted, and recognizes
exactly the set of strings having a suffix accepted by \fIfsm\fR.
The label \fIphi\fR is used to label failure transitions.
\fIphi\fR must be a non-negative integer that is not the label
of any transition in \fIfsm\fR.
If \fIphi = FSMNoLabel\fR,  it will be set to the largest label used in 
\fIfsm\fR plus one.
Like for any Failure class FSM, the only FSM operations that are allowed 
for the output FSM are composition or intersection with non-indexed FSMs.
(Algorithm: constructive)
.PP
.I GRMLocalDeterminize 
takes as input an FSM
\fIfsm\fR and a \fIthreshold\fR integer and returns an equivalent
weighted transducer locally determinized. Unlike the general
determinization case, any FSM is locally
determinizable. Epsilon arcs are treated in the same way as other
symbols. This determinization only eliminates non-determinism locally:
only states with an out-degree greater or equal to \fIthreshold\fR are
considered, and for those states only the non-determinism of outgoing
transitions is eliminated.
(Algorithm: delayed)
.PP
.I GRMSuffix
takes as input an unweighted acceptor \fIfsm\fR 
accepting only one string \fIx\fR and returns
the weighted suffix automaton of \fIx\fR.
(Algorithm: constructive)
.PP
.I GRMCountNgrams
takes as input an FSM \fIifsm\fR and an order \fIorder\fR, and returns
an FSM encoding the \fIcounts\fR of all \fIn\fR-grams appearing in
\fIifsm\fR whose order is less than or equal to \fIorder\fR, where the
\fIcount\fR of an \fIn\fR-gram \fIx\fR is defined as \fI-exp\fR of the
sum of the weights of all paths containing an occurrence of \fIx\fR.
It also takes as input an FSM \fIcfsm\fR, which encodes \fIn\fR-gram
counts to which the counts from \fIifsm\fR are to be added.  If the
argument \fIcfsm\fR is a NULL pointer, a new FSM encoding the
\fIn\fR-gram counts will be created.   The argument \fIsymbol\fR is
the index of the start symbol, and \fIfsymbol\fR is the index
of the final symbol.  When they are greater than zero, the counts
returned are derived from the concatenation of \fIsymbol\fR,
\fIifsm\fR, and \fIfsymbol\fR.  The \fIFSMModes\fR argument applies to
the input FSM \fIifsm\fR. (Algorithm: destructive)
.PP
.I GRMMerge 
takes as input two FSMs: \fIfsm1\fR and \fIfsm2\fR that encode
\fIn\fR-gram counts in the format produced by \fIGRMCount\fR.  It
returns an FSM of the same format that encodes the result of merging
the counts of \fIfsm1\fR and \fIfsm2\fR.  The \fIFSMModes\fR argument
applies to FSM \fIfsm1\fR. (Algorithm: constructive)
.SS STATISTICAL LANGUAGE MODELS
.PP
.I GRMMake 
takes as input an FSM \fIifsm\fR encoding counts of \fIn\fR-grams
counts in the format returned by \fIGRMCount\fR or \fIGRMMerge\fR, and
an order \fIorder\fR for the \fIn\fR-gram model to be made, and
returns an FSM that represents a backoff \fIn\fR-gram model.  It is also
passed an unsigned int that may have any of the 
.I GRMLM 
option bits set as specified in "grmmake.h".  If \fIorder\fR is
greater than the length of the longest \fIn\fR-gram in \fIifsm\fR, or
if \fIorder\fR is less than or equal to zero, \fIorder\fR is set to
the length of the longest \fIn\fR-gram in \fIifsm\fR. (Algorithm:
destructive)
.PP
.I GRMModelShrink
takes as input an FSM \fIifsm\fR representing a backoff \fIn\fR-gram model
in the format produced by \fIGRMMake\fR, and a shrinking parameter
\fIfactor\fR, and returns a backoff \fIn\fR-gram model in the same
format that has been shrunken with respect to \fIfactor\fR.  It is
also passed an unsigned int that may have any of the GRMLM option bits
set as specified in "grmmake.h".  The input FSM \fIifsm\fR must represent
a normalized model.  (Algorithm: destructive)
.PP
.I GRMModelConvert
takes as input an FSM \fIifsm\fR representing a backoff \fIn\fR-gram
model in the format produced by \fIGRMMake\fR or \fIGRMModelShrink\fR,
an index number \fIfsymbol\fR, to which the final symbol will be
relabeled, an index number \fIpsymbol\fR to put on looping arcs at
specified states, and a cost \fIcost\fR for the looping arcs.  It is
also passed an unsigned int that may have any of the GRMLM option bits
set as specified in "grmmake.h".  It returns an FSM that represents an
\fIn\fR-gram model that has been converted from the basic backoff
model.  If the returned FSM is subsequently given as input to
\fIGRMModelShrink\fR\ or \fIGRMModelConvert\fR, the behavior is not
guaranteed.  If \fIfsymbol\fR is less than 0, the final symbol will
not be relabeled.  If \fIpsymbol\fR is less than 0, no looping arcs
will be inserted.  \fIcost\fR must be greater than or equal to 0.
(Algorithm: destructive) 
.SH SEE ALSO
.PD 0
.TP 3.5i
.I grmintro(1)
Intro. to the GRM programs and library.
.TP 3.5i
.I grm(1)
GRM user commands.
.TP 3.5i
.I grm(5) 
GRM file formats.
.TP 3.5i
.I fsmintro(1) 
Intro. to the FSM programs and library.
.PP
.SH FILES
.PD 0
.TP 3.5i
.I /Users/allauzen/lvr/bin/grm-1
Distribution GRM binaries.
.TP 3.5i
.I /Users/allauzen/lvr/src/cmd/grm/grm-1
Distribution GRM sources.
.TP 3.5i
.I /Users/allauzen/lvr/include/grm
Distribution GRM include files.
.TP 3.5i
.I /Users/allauzen/lvr/lib/libgrm-1.{a, so}
Distribution GRM library.
.PP
.SH AUTHOR
Cyril Allauzen (allauzen@research.att.com)
.br
Mehryar Mohri (mohri@research.att.com)
.br
Brian Roark (roark@research.att.com)
.PP
\fBCopyright (C) 2000-2003 AT&T Corp. All rights reserved.
