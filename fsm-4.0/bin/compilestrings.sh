#!/bin/bash

lines=66883
count=2
echo $lines
rm temp.fsm
head -1 $1 | ./farcompilestrings -i ph1.syms | ./fsmcompose - closure.T1.fsm > temp.fsm
while [[ $count -le $lines ]]
do 
    str=`head -$count $1 | tail -1`
    echo $str | ./farcompilestrings -i ph1.syms | ./fsmcompose - closure.T1.fsm >> temp.fsm
    count=`expr $count + 1`
done